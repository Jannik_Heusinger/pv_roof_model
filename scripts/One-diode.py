## One-diode model
import math

### Input variables
# G: total radiation
# Tm: module temperature [K]

# I: Current [A]
# V: Voltage [V]
# Iph: Short-circuit current [A]
# Iph at reference conditions [A] (in data sheet)
# ID: Diode Current
# G: effective irradiance [W/m2]
# Gref: reference irradiance [W/m2]
# Tc: cell temperature [K]
# Tc_ref: reference cell temperature [K]
# mulSC: temperature coefficient of the Short-circuit current [%/K]

def One_diode(G,Tm):

    Gref = 1000
    Tc_ref = 25+273.15
    Isc0= 4.250
    Vmp0= 32.944
    Gamma = 1.35
    Rsh = 550 # shunt resistance
    Rs = 0.36 # series resistance
    mulSC = 0.05
    EGap = 1.12
    Iph_ref = 8.79
    DT0 = 3 # temperature difference between cell and back of module at 1000 W/m2 (K)
    Ncs = 72

    #E = Ibc + fd * Idc
    Tc = Tm + (G/1000.0) * DT0

    Iph = (G/Gref) * (Iph_ref + mulSC*(Tc-Tc_ref))
    Io = Io_ref *(Tc/Tc_ref)**3*math.exp((q*EGap/Gamma * k)*(1/Tc_ref - 1/Tc))

    I = Iph - Io*(math.exp(q*(V+I*Rs)/(Ncs*Gamma*k*Tc))-1) - (V + I * Rs)/Rsh
