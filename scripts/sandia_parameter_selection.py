#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 11 20:44:10 2018

@author: Jannik Heusinger
"""
def sandia_parameter_selection():
    import re
    import pandas as pd

    sandia_input_path = '\\\KLIMA-NAS\\home\\Modelle\\pv_model\\inputs\\Sandia_input_parameters.csv'
    sandia_parameters = pd.read_csv(sandia_input_path)
    sandia_parameter_list = sandia_parameters['Modules'].tolist()

    module = input("Which module should be simulated with the Sandia model (keyword will return a list of options)? ")
    module = module
    regex=re.compile(".*("+module+").*",re.IGNORECASE)
    print("")
    print("These are the available modules:")
    search_results = [m.group(0)for l in sandia_parameter_list for m in [regex.search(l,re.IGNORECASE)] if m]
    print(search_results)
    module_in = input("Please copy and paste the name of the module (without spaces in front of/behind the name) that should be simulated. If there is no suitable module available you can abort this run by pressing control+c and start again by choosing the Masson or One-diode submodules in switches.py. ")

    Module_index = sandia_parameters.loc[sandia_parameters['Modules']=='    '+module_in].index[0]

    print("")
    print("Simulation starts with "+module_in+" module")

    AA = sandia_parameters.iloc[Module_index+1][0]
    AA = float(AA) # Active area [m2]

    NcellSer = sandia_parameters.iloc[Module_index+2][0]
    NcellSer = float(NcellSer) # Number of cells in series

    Isc0 = sandia_parameters.iloc[Module_index+4][0]
    Isc0 = float(Isc0) # Short circuit current

    Imp0 = sandia_parameters.iloc[Module_index+6][0]
    Imp0 = float(Imp0)

    Vmp0 = sandia_parameters.iloc[Module_index+7][0]
    Vmp0 = float(Vmp0)

    aIsc = sandia_parameters.iloc[Module_index+8][0]
    aIsc = float(aIsc)

    aImp = sandia_parameters.iloc[Module_index+9][0]
    aImp = float(aImp)

    C0 = sandia_parameters.iloc[Module_index+10][0]
    C0 = float(C0)
    C1 = sandia_parameters.iloc[Module_index+11][0]
    C1 = float(C1)

    BVmp = sandia_parameters.iloc[Module_index+14][0]
    BVmp = float(BVmp)

    DiodeFactor = sandia_parameters.iloc[Module_index+16][0]
    DiodeFactor = float(DiodeFactor)

    C2 = sandia_parameters.iloc[Module_index+17][0]
    C2 = float(C2)
    C3 = sandia_parameters.iloc[Module_index+18][0]
    C3 = float(C3)
    a0 = sandia_parameters.iloc[Module_index+19][0]
    a0 = float(a0)
    a1 = sandia_parameters.iloc[Module_index+20][0]
    a1 = float(a1)
    a2 = sandia_parameters.iloc[Module_index+21][0]
    a2 = float(a2)
    a3 = sandia_parameters.iloc[Module_index+22][0]
    a3 = float(a3)
    a4 = sandia_parameters.iloc[Module_index+23][0]
    a4 = float(a4)
    b0 = sandia_parameters.iloc[Module_index+24][0]
    b0 = float(b0)
    b1 = sandia_parameters.iloc[Module_index+25][0]
    b1 = float(b1)
    b2 = sandia_parameters.iloc[Module_index+26][0]
    b2 = float(b2)
    b3 = sandia_parameters.iloc[Module_index+27][0]
    b3 = float(b3)
    b4 = sandia_parameters.iloc[Module_index+28][0]
    b4 = float(b4)
    b5 = sandia_parameters.iloc[Module_index+29][0]
    b5 = float(b5)
    DT0 = sandia_parameters.iloc[Module_index+30][0]
    DT0 = float(DT0)
    fd = sandia_parameters.iloc[Module_index+31][0]
    fd = float(fd)
    a = sandia_parameters.iloc[Module_index+32][0]
    a = float(a)
    b= sandia_parameters.iloc[Module_index+33][0]
    b = float(b)

    return AA,NcellSer, Isc0, Imp0, Vmp0, aIsc, aImp, C0, C1, BVmp, DiodeFactor, C2, C3, a0, a1, a2, a3, a4, b0, b1, b2, b3, b4, b5, DT0, fd, a, b
