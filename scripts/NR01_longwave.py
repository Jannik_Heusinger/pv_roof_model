# NR01 longwave radiation FOV
import math
import csv
import pandas as pd
import numpy as np
from constants import width_rows,L,h,h_NR01


def sensor_raytracing(beta_PV,S1_EW,S2_EW,S_length_EW,gamma_sh):
    #Output_table = pd.DataFrame(columns = ['width_rows*j','Lview_EW','S1_EW','U/2','Side'])
    #Output_table2 = pd.DataFrame(columns = ['horizontal_dist_lowh','horizontal_dist_highh','h_loweredge','h_upperedge','PV_distance_low',
    #'PV_distance_high','x','y'])

    h_panel = math.sin(beta_PV*math.pi/180)*L
    h_upperedge =h+h_panel/2
    h_loweredge = h-h_panel/2
    U = L*math.cos(beta_PV*math.pi/180)
    FOV = math.ceil(h_NR01*math.tan(75*math.pi/180)) # radius of the field of view (m) of the sensor
    Surface_indicator=[] # 1 = PV module ; 2= Ground shaded; 3 = Ground unshaded
    PV_only_angle = math.acos(U/2/FOV)/math.pi*180 # this is the angle above which the surface_indicator is always 1

    shade_begin =[S2_EW-U/2] # vector that contains the horizontal distances between the middle of the PV direclty below the sensor and the beginning of a shade
                            # in orientation to the upper edge of the panel
    shade_end = [S2_EW-U/2+S_length_EW] # same for distances where shades end
    i = 1
    while (shade_begin[i-1]<FOV+L):
        shade_begin.append(width_rows*i+shade_begin[i-1])
        shade_end.append(width_rows*i+shade_end[i-1])
        i = i+1

    # shade distances vector in orientation to the lower edge of the panel
    if S2_EW < U/2:
        shade_begin1 = [0]
        shade_end1 = [U/2-S2_EW]
    else:
        shade_begin1 = [width_rows-U/2-S1_EW]
        shade_end1 = [shade_begin1[0]+S_length_EW]
    i = 1
    while (shade_begin1[i-1]<FOV+L):
        shade_begin1.append(width_rows*i+shade_begin[i-1])
        shade_end1.append(width_rows*i+shade_end[i-1])
        i = i+1

    # calculate surface indicator within the horizontal FOV circle
    for psi in range(0,90): # horizontal angle in 1 degree steps
        #horizontal distances between the sensor and the lower as well as upper edges of the PV (projected to the ground)
        # within the field of view of the sensor
        PV_distance_low =[0]
        PV_distance_high = []
        i = 1
        while (PV_distance_low[i-1]<FOV+L):
            PV_distance_low.append((width_rows*i-U/2)/math.cos(psi*math.pi/180))
            i = i+1

        i = 0
        PV_distance_high.append((width_rows*i+U/2)/math.cos(psi*math.pi/180))
        while (PV_distance_high[i]<FOV+L):
            PV_distance_high.append((width_rows*(i+1)+U/2)/math.cos(psi*math.pi/180))
            i = i+1

        # calculate the surface indicator in 0.1 m steps for the left and right side
        for i in range(1,FOV*10):
            if psi >= PV_only_angle:
                Surface_indicator.append(1)
            else:
                NR01_angle = math.atan(i*0.1/h_NR01)/math.pi*180 # calculate the angle
                # calculate the horizontal distance between the sensor and the origin of the ray
                Lview = h_NR01 *math.tan(NR01_angle*math.pi/180)
                # Lview projected on an east-west axis
                Lview_EW = Lview*math.cos(psi*math.pi/180)
                # calculate horizontal distance at the height of the lower PV module edge
                horizontal_dist_lowh = (h_NR01-h_loweredge)*math.tan(NR01_angle*math.pi/180)
                # calculate horizontal distance at the height of the upper PV module edge
                horizontal_dist_highh = (h_NR01-h_upperedge)*math.tan(NR01_angle*math.pi/180)
                # calculate horizontal distance at ground level
                horizontal_dist_ground = h_NR01*math.tan(NR01_angle*math.pi/180)
                for j in range(0,len(PV_distance_high)): # j = row;
                # this is for one (western) half of the 150° FOV angle; the other half is symmetric to that, which means that PV_distance_low becomes PV_distance_high
                # and horizontal_dist_lowh becomes horizontal_dist_highh and vice versa

                    # calculate the intersection of both lines (Lup ray(NRO1_angle) and PV_module(beta_PV))
                    xB1 = horizontal_dist_lowh
                    xB2 = horizontal_dist_highh
                    yB1 = h_loweredge
                    yB2 = h_upperedge
                    xR1 = PV_distance_low[j]
                    xR2 = PV_distance_high[j]
                    yR1 = h_loweredge
                    yR2 = h_upperedge
                    # calculate slopes
                    m_ray = (yB2 - yB1)/(xB2 - xB1)
                    m_PV =  (yB2 - yB1)/(xR2 - xR1)
                    # point of intersection
                    x = (-xB2*xR1*yB1 + xB2*xR2*yB1 + xB1*xR1*yB2 - xB1*xR2*yB2 + xB1*xR2*yR1 - xB2*xR2*yR1 - xB1*xR1*yR2 + xB2*xR1*yR2)/(-xR1*yB1 +
                    xR2*yB1 + xR1*yB2 - xR2*yB2 + xB1*yR1 - xB2*yR1 - xB1*yR2 + xB2*yR2)
                    y = (yB2 - yB1)/(xB2 - xB1)*(x - xB1) + yB1
                    #print(x,y)
                    if x <= PV_distance_high[j] and x >= PV_distance_low[j]:
                        if y <= h_upperedge and y >= h_loweredge:
                            m_PV_sign = np.sign(m_PV)
                            m_ray_sign = np.sign(m_ray)
                            if m_PV_sign != m_ray_sign:
                                Surface_indicator.append(1) # upper PV surface
                            elif abs(m_PV)>=abs(m_ray):
                                Surface_indicator.append(2) # lower PV surface
                            else:
                                Surface_indicator(1) # upper PV surface
                            break
                    if j==len(PV_distance_high)-1:
                        if gamma_sh<=0: # solar azimuth (negative before noon)
                            for k in range(len(shade_begin)):
                                if Lview_EW >= shade_begin[k] and Lview_EW <= shade_end[k]:
                                    Surface_indicator.append(3) # ground is shaded
                                    break
                            Surface_indicator.append(4) # ground is unshaded
                        elif gamma_sh>0:
                            for k in range(len(shade_begin1)):
                                if Lview_EW >= shade_begin1[k] and Lview_EW <= shade_end1[k]:
                                    Surface_indicator.append(3) # ground is shaded
                                    break
                            Surface_indicator.append(4) # ground is unshaded
                            #Output_table= Output_table.append({'width_rows*j':width_rows*j,'Lview_EW':Lview_EW,'S1_EW':S1_EW,'U/2':U/2,'Side':'Left'},ignore_index=True)


                for j in range(0,len(PV_distance_high)): # j = row;
                    # this is the other half of the 150° FOV angle;

                    # calculate the intersection of both lines (Lup ray(NRO1_angle) and PV_module(beta_PV))
                    xB1 = horizontal_dist_lowh
                    xB2 = horizontal_dist_highh
                    yB1 = h_loweredge
                    yB2 = h_upperedge
                    xR1 = PV_distance_high[j]
                    xR2 = PV_distance_low[j]
                    yR1 = h_loweredge
                    yR2 = h_upperedge
                    # calculate slopes
                    m_ray = (yB2 - yB1)/(xB2 - xB1)
                    m_PV =  (yB2 - yB1)/(xR1 - xR2)
                    try:
                        x = (-xB2*xR1*yB1 + xB2*xR2*yB1 + xB1*xR1*yB2 - xB1*xR2*yB2 + xB1*xR2*yR1 - xB2*xR2*yR1 - xB1*xR1*yR2 + xB2*xR1*yR2)/(-xR1*yB1 +
                        xR2*yB1 + xR1*yB2 - xR2*yB2 + xB1*yR1 - xB2*yR1 - xB1*yR2 + xB2*yR2)
                        y = (yB2 - yB1)/(xB2 - xB1)*(x - xB1) + yB1
                    except:
                        print('float division by zero')
                        continue
                    #Output_table2 = Output_table2.append({'horizontal_dist_lowh':xB2,'horizontal_dist_highh':xB1,'h_loweredge':yB1,'h_upperedge':yB2,'PV_distance_low':xR2,
                    #'PV_distance_high':xR1,'x':x,'y':y},ignore_index=True)

                    #print(x,y)
                    if x >= PV_distance_low[j] and x <= PV_distance_high[j]:
                        if y <= h_upperedge and y >= h_loweredge:
                            m_PV_sign = np.sign(m_PV)
                            m_ray_sign = np.sign(m_ray)
                            if m_PV_sign != m_ray_sign:
                                Surface_indicator.append(1) # upper PV surface
                            elif abs(m_PV)>=abs(m_ray):
                                Surface_indicator.append(2) # lower PV surface
                            else:
                                Surface_indicator(1) # upper PV surface
                            break
                    if j==len(PV_distance_high)-1:
                        if gamma_sh>0: # solar azimuth (negative before noon)
                            for k in range(len(shade_begin)):
                                if Lview_EW >= shade_begin[k] and Lview_EW <= shade_end[k]:
                                    Surface_indicator.append(3) # ground is shaded
                                    break
                            Surface_indicator.append(4) # ground is unshaded
                        elif gamma_sh<=0:
                            for k in range(len(shade_begin1)):
                                if Lview_EW >= shade_begin1[k] and Lview_EW <= shade_end1[k]:
                                    Surface_indicator.append(3) # ground is shaded
                                    break
                            Surface_indicator.append(4) # ground is unshaded

    surface_count_PV_up = 0 # upper PV surface
    surface_count_PV_down = 0 # lower PV surface
    surface_count_ground_shaded = 0
    surface_count_ground_unshaded = 0
    for i in range(0,len(Surface_indicator)):
        if Surface_indicator[i]==1:
            surface_count_PV_up=surface_count_PV_up+1
        elif Surface_indicator[i]==2:
            surface_count_PV_down=surface_count_PV_down+1
        elif Surface_indicator[i]==3:
            surface_count_ground_shaded = surface_count_ground_shaded+1
        elif Surface_indicator[i]==4:
            surface_count_ground_unshaded = surface_count_ground_unshaded+1

    PV_surface_up_fraction = surface_count_PV_up/len(Surface_indicator)
    PV_surface_down_fraction = surface_count_PV_down/len(Surface_indicator)
    Ground_shaded_fraction = surface_count_ground_shaded/len(Surface_indicator)
    Ground_unshaded_fraction = surface_count_ground_unshaded/len(Surface_indicator)

    #Output_table.to_csv('../outputs/Raytracing_surface_fractions_test.csv')
    #Output_table2.to_csv('../outputs/Raytracing_surface_fractions_test2.csv')
    return PV_surface_up_fraction,PV_surface_down_fraction,Ground_shaded_fraction,Ground_unshaded_fraction
