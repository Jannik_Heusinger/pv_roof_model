"""
calculates surface temperature using force-restore method (Jacobs, 2000)
 
see section 3.3 tech notes for details

inputs:
    eng_bal     = energy balance dictionary
    cs          = constants dictionary
    cfM         = main control file
    mod_ts      = modelled surface temperature dataframe
    mod_tm      = modelled ground temperature dataframe 
    surf        = current surface type   
    Dats        = dates dictionary
    i           = current index 
    
Outputs:
    Ts = modelled surface temperature (Tsurf)
    Tm = modelled soil (ground)  temperature (Tm)
 
 
 
"""
import math
from datetime import timedelta
from constants import *

def Ts_calc_surf(QGS,tm,ts):

    D = math.sqrt((2.*K)/((2.*math.pi)/86400.)) # the damping depth for the annual temperature cycle
    Dy = D * math.sqrt(365.)    

 
    delta_Tg = ((2./(C*D)*QGS))-(((2.*math.pi)/86400.)*(ts-tm)) ## the change in Tsurf per second 
    delta_Tm = QGS/(C*Dy)       ## change in Tm per second
 
    return {delta_Tg, delta_Tm}





