# This file defines the model setup, input/output data file path and submodules to be used
from constants import *

# input data file path
input_path = '//KLIMA-NAS/home/Modelle/pv_model/inputs/PV_roof_input.csv'

#output data file path
output_path = '//KLIMA-NAS/home/Modelle/pv_model/outputs/PV_roof_output.csv'

# Measured ground surface temperatures(0), force-restore model (1) or finite difference scheme (2)
Tsurf_switch = 0

if Tsurf_switch ==2:
    # Compute stable timestep
    dt_stable = dz**2/kappa_c/4
    dt_CFL = (kappa_c*dt)/dz**2
    print('CFL number = '+str(dt_CFL))
    if dt > dt_stable:
        print('Warning: decrease time step dt - ground surface temperatures are unstable')

# Use global horizontal radiation input data (1) or simulate clear sky radiation (2)
SW_down_switch = 1

# Choose clear sky radiation model: 'Bird' or 'Ineichen'
clear_sky_type = 'Bird'

# Choose diffuse radiation model: 'isotropic' or 'perez'
diffuse_model = 'isotropic'

# Tracking type of the system: 1=Flat, 2=tilted,fixed, 3= 1-axis (horizontal axis), 4= 1-axis (sloped), 5= 1-axis (vertical), 6= 2-axis
tracking_type = 1

# Choose PV type: Monocrystalline or Polycrystalline or Thinfilm
PV_type = 'Thinfilm'
# PV module rear side insulated? 0= no insulation, 1= insulated
Insulated = 0

# Choose power model: 'Masson' or 'Sandia' or 'Batzelis' or 'Notton'
# Batzelis is an analytical solution to the One diode model (Batzelis 2017)
# Notton is the parameterization presented in Notton et al. 2005
# Sandia works only for a selection of specific PV modules which are available in the Sandia PV model database
Power_mod = 'Masson'

# Choose to calculate averaged rooftop PV temperatures/QH ('yes') or local PV panel temperature/QH ('no')
Rooftop_average = 'no'
Rooftop_average_method = 'TARP'  # TARP or polynomials

# PV module material characteristics
# List for all materials contains: thickness (d; m), density (rho; kg/m^3), specific heat capacity (C; J/(kg*K) )
# After Davis et al. 2003
Monocrystalline = {'Glass':[0.006,2500,840],'Cell':[0.00086,2330,712],
                    'Backing':[0.00017,1475,1130],'Insulation':[0.1016,55,1210]}
Polycrystalline = {'Glass':[0.006,2500,840],'Cell':[0.00038,2330,712],'Backing':
                    [0.00017,1475,1130],'Insulation':[0.1016,55,1210]}
Thinfilm = {'Glass':[0.000051,1750,1050],'Cell':[0.000001,2330,712],'Backing':
                     [0.000125,7900,477],'Insulation':[0.1016,55,1210]}
