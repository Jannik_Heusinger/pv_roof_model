# 1D Finite difference scheme to solve for the surface temperature in the desert
# latent heat fluxes are neglected for now
# Neumann conditions at the upper and lower boundaries
# at the lower boundary the heat flux is assumed to be 0

from constants import *

def FD_Tsurf(T,TB):
    # Thermal diffusivity on vertical grid
    kappa = [kappa_c]*nz

    # explicit model, forward in time, central in space

    # Compute new temperature
    Tnew = [0]*nz
    sz = kappa[nz-1]*dt/dz**2
    for i in range(0,nz-1):

        Tnew[i]=T[i]+sz*(T[i+1]-2*T[i]+T[i-1])
        Tnew[0]= TB
        Tnew[nz-1]=T[nz-1]
        # print('Tnrew[0] = '+str(Tnew[0]))
        # print('Tnew[nz-1] = '+str(Tnew[nz-1]))
        # print('T[nz-1] = '+str(T[nz-1]))
        # print('T[nz-2] = '+str(T[nz-2]))

    # update temperature and time
    T = Tnew
    return T
