# PV array surface energy balance model

from lookup_dates import lookup
from shade_frc import shade_frc_fct
from NR01_longwave import sensor_raytracing
from solar_geo_array import solar_geo_array
from constants import *
import math
import pandas as pd
import numpy as np

#############
# Read data
#############

data = pd.read_csv('../inputs/Red_Rock_input.csv',infer_datetime_format=True)
data_Tmodule = pd.read_csv('../outputs/PVmodel_output.csv',infer_datetime_format=True)
data_ref = pd.read_csv('../inputs/ref_input_data_RedRock.csv',infer_datetime_format=True)

data['Datetime'] = lookup(data['Datetime'])
data_Tmodule['Datetime'] = data['Datetime']
data_ref['Datetime'] = data['Datetime']

# calculate day of year
data['DOY'] = data['Datetime'].dt.dayofyear
# LH: local hour (1-24)
data['hour'] = data['Datetime'].dt.hour
data = data.set_index(['Datetime'])

data_Tmodule = data_Tmodule.set_index(['Datetime'])
data_ref = data_ref.set_index(['Datetime'])

# resample to hourly timestep for solar_geo function
data_hourly = data.resample('60T').asfreq()
data_Tmodule_hourly = data_Tmodule.resample('60T').asfreq()
data_ref_hourly = data_ref.resample('60T').asfreq()

doy = data_hourly['DOY'].tolist()
hour = data_hourly['hour'].tolist()
hour = np.array(hour)
hour = hour+0.5
LH = hour.tolist()
global_rad = data_hourly['Fsd_nr01_Avg'].tolist()
global_rad_hourly = np.array(global_rad)
global_rad_hourly_MJ = global_rad_hourly*3600./1000000.
global_rad_hourly_MJ = global_rad_hourly_MJ.tolist()

Tground_sun = data_hourly['T_surface_est_sun'].tolist()
Tground_sun = np.array(Tground_sun)
Tground_sun = Tground_sun+273.15 # element-wise
Tground_sun = Tground_sun.tolist()

Tground_shade = data_hourly['T_surface_est_shade'].tolist()
Tground_shade = np.array(Tground_shade)
Tground_shade = Tground_shade+273.15 # element-wise
Tground_shade = Tground_shade.tolist()

TA = data_hourly['Tair_1.5m'].tolist()
TA = np.array(TA)
TA = TA+273.15 # element-wise
TA = TA.tolist()
Rn_meas = data_hourly['Rn'].tolist()
L_up_meas = data_hourly['Flu_nr01_Avg'].tolist()
L_down_meas = data_hourly['Fld_nr01_Avg'].tolist()
S_up_meas = data_hourly['Fsu_nr01_Avg'].tolist()
Tmodule = data_Tmodule_hourly['T_module'].tolist()

Tground_ref = data_ref_hourly['T_surface_est'].tolist()
Tground_ref = np.array(Tground_ref)
Tground_ref = Tground_ref+273.15 # element-wise
Tground_ref = Tground_ref.tolist()
L_up_ref_meas = data_ref_hourly['Flu_nr01_Avg'].tolist()
S_up_ref_meas = data_ref_hourly['Fsu_nr01_Avg'].tolist()
Rn_ref_meas = data_ref_hourly['Rn'].tolist()



# Define an output table for collecting model results
Output_table = pd.DataFrame(columns = ['S_length','S_length_EW','cos_theta_zh','gamma_sh','shade_frc','Rn','Rn_meas','Lup','Lup_meas','Ldown','Ldown_meas','Sup','Sup_meas',
'TA','Tground_ref','S_up_ref','S_up_ref_meas','L_up_ref','L_up_ref_meas','Rn_ref','Rn_ref_meas','PVF_up','PVF_down','GSF','GUF','beta_PV','S1_EW'])

for i in range(0,len(doy)):
    print(i/len(doy))
    sky_condition =1
    if sky_condition ==1:
        em_sky = 0.95  # clear sky emissivity
        T_sky = TA[i]-20
    elif sky_condition ==0:
        em_sky = 1  # overcast sky emissivity
        T_sky = TA[i]

    omega,sinalpha,solar_alt,omega_sunrise,omega_sunset,cos_theta_zh,theta_zh,gamma_sh,kt = solar_geo(Lat,Lon,doy[i],LH[i],TZ,global_rad_hourly_MJ[i])

    ## surface azimuth
    # for tracking system with horizontal axis, which is north-south oriented
    if omega < 0:
        gamma_h = -90
    else:
        gamma_h = 90

    ## tilt angle (after Braun and Mitchell 1983)
    beta_0 = math.atan(math.tan(theta_zh*math.pi/180)*math.cos((gamma_h-gamma_sh)*math.pi/180))/math.pi*180
    if beta_0 >= 0:
        sigma_beta = 0
    else:
        sigma_beta = 1

    beta_PV = beta_0 + sigma_beta*180
    if beta_PV > 60:
        beta_PV = 60
    if cos_theta_zh <= 0:
        beta_PV = 30

    # calculate the shaded fraction inside the PV array
    shade_frc,U,S_length,S_length_EW,S2_EW,B,S1_EW = shade_frc_fct(gamma_sh,beta_PV,solar_alt,cos_theta_zh)

    # PV and ground fractions that are "seen" by the NR01
    # Polynomial was fitted based on results from raytracing.py
    PVF = -2e-07*beta_PV**3-2e-05*beta_PV**2+0.0037*beta_PV+0.1906 # fraction of PV that is seen by NR01 sensor

    # calculate the fraction of the shaded/unshaded ground that is seen by NR01
    PVF_up,PVF_down,GSF,GUF = sensor_raytracing(beta_PV,S1_EW,S2_EW,S_length_EW,gamma_sh)

    # surface radiation balance (multiple reflections between ground and module not implemented here)
    # of the PV array
    PLF = U/width_rows          # plan area fraction of PV modules
    S_up_PV = PV_albedo*global_rad[i]*shade_frc
    S_up_ground = ground_albedo*global_rad[i]*(1-shade_frc)
    S_up = S_up_PV+S_up_ground
    L_down = sigma*em_sky*T_sky**4
    L_up_PV = ((sigma*em_module_up*Tmodule[i]**4)+((1-em_module_up)*L_down_meas[i]))*PVF_up + ((sigma*em_module_down*Tmodule[i]**4)+((1-em_module_down)*L_down_meas[i]))*PVF_down# contains longwave rad after Stefan-Boltzmann and reflected longwave rad
    L_up_ground = sigma*(em_ground*Tground_sun[i]**4*(GUF)+em_ground*Tground_shade[i]**4*GSF)+(1-em_ground)*L_down_meas[i]*(1-PVF) # contains longwave rad after Stefan-Boltzmann and reflected longwave rad
    L_up = L_up_PV+L_up_ground
    Rn = global_rad[i]-S_up+L_down_meas[i]-L_up

    # of the ref site
    S_up_ref = ground_albedo*global_rad[i]
    L_up_ref = sigma*em_ground*Tground_ref[i]**4
    Rn_ref = global_rad[i]-S_up_ref+L_down-L_up_ref

    Output_table= Output_table.append({'S_length':S_length,'S_length_EW':S_length_EW,'cos_theta_zh':cos_theta_zh,'gamma_sh':gamma_sh,'shade_frc':shade_frc,'Rn':Rn,'Rn_meas':Rn_meas[i],
    'Lup':L_up,'Lup_meas':L_up_meas[i],'Ldown':L_down,'Ldown_meas':L_down_meas[i],'Sup':S_up,'Sup_meas':S_up_meas[i],'TA':TA[i],'Tground_ref':Tground_ref[i],'S_up_ref':S_up_ref,
    'S_up_ref_meas':S_up_ref_meas[i],'L_up_ref':L_up_ref,'L_up_ref_meas':L_up_ref_meas[i],'Rn_ref':Rn_ref,'Rn_ref_meas':Rn_ref_meas[i],'PVF_up':PVF_up,'PVF_down':PVF_down,'GSF':GSF,'GUF':GUF,'beta_PV':beta_PV,'S1_EW':S1_EW},ignore_index=True)
print(Output_table)
Output_table.to_csv('../outputs/Surface_eb_output.csv')
