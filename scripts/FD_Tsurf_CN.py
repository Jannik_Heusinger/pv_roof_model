import numpy as np
from scipy.sparse import spdiags
import matplotlib.pyplot as plt
import math
from constants import *


def FD_Tsurf(T,TB):
    L = 1
    imax=nz
    X = np.linspace(0,L,imax)
    f0 = T
    nstep = 15
    D = kappa_c
    alpha = D*dt/(dz)**2

    # constructing tri-diagonal matrizes for solving 1D heat eq with crank nicolson
    B = np.array([[-alpha]*imax,[2*(1+ alpha)]*imax,[-alpha]*imax])
    diags = np.array([-1,0,1])
    Lx = spdiags(B,diags,imax, imax).toarray()
    B = np.array([[alpha]*imax,[2*(1- alpha)]*imax,[alpha]*imax])
    Rx = spdiags(B,diags,imax, imax).toarray()

    # preparing matrizes for Dirichlet boundary conditions
    Lx[0,0] = 1
    Lx[0,1] = 0
    Lx[-1,-1] = 1
    Lx[-1,-2] = 0

    Rx[0,0] = 1
    Rx[0,1] = 0
    Rx[-1,-1] = 1
    Rx[-1,-2] = 0
    u = np.array(f0)
    # solve inner gridpoints
    bb = Rx.dot(u)
    u = np.linalg.solve(Lx,bb)
    # set boundary values
    u[0] = TB
    u[-1] = T[-1]
    Tnew = u
    return Tnew
