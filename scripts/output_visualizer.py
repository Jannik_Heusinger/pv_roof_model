# Script to visualize output from PV model
from switches import *
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.offsetbox import AnchoredText
import numpy as np

# path where validation data is stored
validation_path = 'modify/this/path'

# read data
model_data = pd.read_csv(output_path,infer_datetime_format=True)
validation_data = pd.read_excel(validation_path,index_col=1,infer_datetime_format=True)

Tmodule_meas1 = validation_data['Temp_C_Panel1']
Tmodule_meas2 = validation_data['Temp_C_Panel2']
Temp_df = pd.concat([Tmodule_meas1, Tmodule_meas2], axis=1)
Tmodule_meas = Temp_df.mean(axis = 1)

Tmodule_hat = model_data['T_module']-273.15
Tmodule_meas = Tmodule_meas[0:len(Tmodule_hat)]
Tmodule_hat.index = Tmodule_meas.index

# timeseries plot
plt.plot(Tmodule_meas)
plt.plot(Tmodule_hat)
plt.show()

# linear regression fit for scatter plot
model = np.polyfit(Tmodule_hat, Tmodule_meas, 1)
m = round(model[0], 2)
n = round(model[1], 2)
x = range(int(round(min(Tmodule_meas)-5,1)),int(round(max(Tmodule_meas)+5,1)))
y = m*x+n

# calculate coefficient of determination
p = np.poly1d(model)
# fit values, and mean
yhat = p(x)                         # or [p(z) for z in x]
ybar = np.sum(y)/len(y)          # or sum(y)/len(y)
ssreg = np.sum((yhat-ybar)**2)   # or sum([ (yihat - ybar)**2 for yihat in yhat])
sstot = np.sum((y - ybar)**2)    # or sum([ (yi - ybar)**2 for yi in y])
r_squared= round(ssreg / sstot,2)

# calculate RMSE
e = Tmodule_hat - Tmodule_meas
RMSE = round(np.mean(np.sqrt(e**2)),2)

one_one_line = range(int(round(min(Tmodule_meas)-10,1)),int(round(max(Tmodule_meas)+10,1)))
fig, ax = plt.subplots(1, 1)
at = AnchoredText(
    "y = "+str(m)+"x + "+str(n)+"\n$R^{2}$ = "+str(r_squared)+"\nRMSE = "+str(RMSE), prop=dict(size=10), frameon=True, loc='upper left')
at.patch.set_boxstyle("round,pad=0.,rounding_size=0.2")
ax.add_artist(at)
ax.scatter(Tmodule_hat,Tmodule_meas,alpha=0.5,edgecolors='none')
ax.plot(x,y,"C1")
ax.plot(one_one_line,one_one_line,'--',color="0.3")
ax.set_xlabel('Modeled $T_{PV}$ (°C)')
ax.set_ylabel('Measured $T_{PV}$ (°C)')
plt.show()
