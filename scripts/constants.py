# This file contains all constants needed for
# PV_model_MASTER.py, PVarray_surface_eb.py and NR01_longwave.py

dt=30           # time step (s)
inputdt = 1800          # time step of input data (s)
outputdt = 1800         # time step for output (s)

A = 1                   # surface area (m^2)
#rho_s = 0.2
z0 = 18*0.1             # roughness length of mean building height in the surroundings
Roof_size = 1600        # roof size - needed when Rooftop_average = 'yes' in switches.py
Roof_shape = 'square'   # shape can be 'square' or 'rectangle' - only relevant if Rooftop_average_method = 'polynomials'

### PV characteristics ###
alpha = 0.94           # absorptivity of the cell surface (below glazing)


PV_albedo_min = 0.05             # reflectivity of the PV surface
#n = 1.526               # refraction index of the cover glass
n = 1.398               # refraction index of Tefzel (cover of Thinfilm panels)
K_glazing = 4           # glazing extinction coefficient (m-1)
em_module_up = 0.79     # PV module upper surface emissivity
em_module_down = 0.95   # PV module lower surface emissivity
h = 18.1            # PV height above ground (of horizontal axis, m)
L = 1.1096          # length of PV module perpendicular to tilt axis (m)
W = 1.3096            # width of PV module, necessary for calculating Pout and 2 axes shade fraction calc

#L=20                 # for roof calculations roof length otherwise panel length
#W=20                 # for roof calculations roof width otherwise panel width
L_char = 4*L*W/(2*L+2*W) # characteristic length for sens. heat flux calculations (m)

# data sheet specs (needed for Batzelis One-diode model - only works for crystalline modules)
alpha_Isc = 0.001  # temperature coefficient Isc (°C-1)
beta_Voc = -0.003  # temperature coefficient Voc (°C-1)
Voc0 = 131.5       # open circuit voltage (V)
Isc0 = 1.037       # open circuit current (A)
Imp0 = 0.914       # Current at max power (A)
Vmp0 = 104.1       # Voltage at max power (V)


### tracking system related constants ###
beta_max = 60           # maximum tilt angle of PV (horixontal 1-axis tracking system)
beta_night = 30         # tilt angle of PV at night (horixontal 1-axis tracking system)
beta_tilt = 10          # tilt angle for non-tracking and vertical axis tracking
gamma_h_1 = -15         # azimuth angle of fixed, tilted system (0 facing equator, west 90, east -90)
beta_dash = 15          # tilt angle of sloped axis (sloped 1-axis tracking system)
gamma_sloped = 0        # azimuth angle of sloped axis (0 facing equator, west 90, east -90)


### Location and time zone ###
Lat = 52.273126
Lon = 10.534298
#Lat = 32.558558
#Lon = -111.290814
TZ = -1              # CET relative to UTC

### Universal constants ###
sigma = 5.669*10**(-8)  # Stefan-Boltzmann constant
Rv = 461.0              # Gas constant for water vapour [J K‐1 kg-1]

### PV power output ###
Eff_PV = 0.065           # conversion efficiency of the PV module (STC)
#Eff_PV = 0.18
CFF = 1.22              # fill factor model constant (K m^2)
k1 = 10**6              # constant=K/I0 (m^2 K)

### PV array ###
width_rows = 5.64   # distance between rows (measured from one axis to the other, m)
#width_rows = 0.01
length_rows = 200   # length of PV panel rows in array (m)
h_NR01 = 6          # height of radiation sensor (m)

### Roof surface ###
albedo_roof = 0.3      # roof albedo - fiberglass
em_roof = 0.7        # roof emissivity - fiberglass

####### LUMPS (OHM) parameters ########
a1=0.35
a2=0.43
a3=-36.5

###### Force restore model parameters ######
C = 1350000.  # heat capacity of ground (J m^-3 K^-1)
K = 0.00000021 # thermal diffusivity of ground (m^2 s^-1)

###### Finite difference soil temp parameters #####
L_dom= 0.0025 # length of model domain (m) - roof thickness
nz = 3     # number of gridpoints in z-direction

k = 0.04    # heat conductivity fiberglass (W m^-1 K^-1)
Cp = 700    # heat capacity (J kg-1 K-1)desert sand
ro = 2 # density (kg m-3) fiberglass
Croof = L_dom*ro*Cp
#ro = 2400 # concrete
kappa_c = k/(ro*Cp) # thermal diffusivity (m2 s^-1)
#kappa_c = 0.24E-06 # m2 s-1
dz = L_dom/(nz-1) # Spacing of gridpoints

ks = 0.12
Cps = 921
ros = 1520
kappa_s = ks/(ros*Cps)


##### Atmospheric constants ######
v_kin = 17.7*10**(-6)   # kinematic viscosity of air (m^2 s^-1)
kf = 0.025  # thermal conductivity of air (W m^-1 K^-1)
Pr = 0.71   # Prandtl number for air
