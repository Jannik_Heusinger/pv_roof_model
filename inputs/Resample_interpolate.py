# Script to resample and interpolate data
import pandas as pd
from lookup_dates import lookup 

###########################################
# Read and interpolate meteorological input to timestep
###########################################
dt = 1800 # time step to be resampled (sec)
data = pd.read_csv('../inputs/PV_electricity_production.csv',infer_datetime_format=True)
#data = pd.read_csv('../inputs/Red_Rock_input.csv',infer_datetime_format=True)

data['Datetime'] = lookup(data['Datetime'])
data = data.set_index(['Datetime'])


# now resample and interpolate to model timestep
dt_str = str(dt)
data_dt_1 = data.resample(''+dt_str+'S').asfreq()
data_dt_1 = data_dt_1.interpolate() # interpolated data to timestep dt

data_dt_1.to_csv('../inputs/resampled_data.csv')
